type fruitKind =
    | Apple
    | Lemon
    | Lime;

type fruitColor =
  | Green
  | Red
  | Yellow;

type fruit = {
    color: fruitColor,
    kind: fruitKind,
    weight: int
}

let fruits: list(fruit) = [
    { color: Green, kind: Apple, weight: 15 },
    { color: Red, kind: Apple, weight: 11 },
    { color: Green, kind: Apple, weight: 30 },
    { color: Red, kind: Apple, weight: 23 },
    { color: Green, kind: Apple, weight: 23 },
    { color: Green, kind: Apple, weight: 32 },
    { color: Green, kind: Lime, weight: 30 },
    { color: Green, kind: Lime, weight: 20 },
    { color: Green, kind: Lime, weight: 30 },
    { color: Yellow, kind: Lemon, weight: 10 },
    { color: Yellow, kind: Lemon, weight: 30 },
    { color: Green, kind: Lemon, weight: 10 },
    { color: Green, kind: Lemon, weight: 12 },
    { color: Yellow, kind: Lemon, weight: 31 },
    { color: Yellow, kind: Lemon, weight: 10 },
    { color: Yellow, kind: Lemon, weight: 32 }
];