let boolToString = (b: bool): string => b ? "--->TRUE<---" : "--->FALSE<----";

let (testRedApples: bool) = FruitBlender.onlyRedApples(Fruit.fruits) == [
    { color: Red, kind: Apple, weight: 11, },
    { color: Red, kind: Apple, weight: 23, }
];

let (testGreenApples: bool) = FruitBlender.onlyGreenApples(Fruit.fruits) == [
    { color: Green, kind: Apple, weight: 15, },
    { color: Green, kind: Apple, weight: 30, },
    { color: Green, kind: Apple, weight: 23, },
    { color: Green, kind: Apple, weight: 32, }
];

let (testYellowLemons: bool) = FruitBlender.onlyYellowLemons(Fruit.fruits) == [
    { color: Yellow, kind: Lemon, weight: 10, },
    { color: Yellow, kind: Lemon, weight: 30, },
    { color: Yellow, kind: Lemon, weight: 31, },
    { color: Yellow, kind: Lemon, weight: 10, },
    { color: Yellow, kind: Lemon, weight: 32, } 
];

let (testOnlyApples: bool) = FruitBlender.onlyApples(Fruit.fruits) == [
    { color: Green, kind: Apple, weight: 15, },
    { color: Red, kind: Apple, weight: 11, },
    { color: Green, kind: Apple, weight: 30, },
    { color: Red, kind: Apple, weight: 23, },
    { color: Green, kind: Apple, weight: 23, },
    { color: Green, kind: Apple, weight: 32, }
];

let (testOnlyLemons: bool) = FruitBlender.onlyLemons(Fruit.fruits) == [
    { color: Yellow, kind: Lemon, weight: 10, },
    { color: Yellow, kind: Lemon, weight: 30, },
    { color: Green, kind: Lemon, weight: 10, },
    { color: Green, kind: Lemon, weight: 12, },
    { color: Yellow, kind: Lemon, weight: 31, },
    { color: Yellow, kind: Lemon, weight: 10, },
    { color: Yellow, kind: Lemon, weight: 32, } 
];

let (testHeavyFruits: bool) = FruitBlender.onlyHeavyFruits(Fruit.fruits) == [
    { color: Green, kind: Apple, weight: 30 },
    { color: Green, kind: Apple, weight: 32 },
    { color: Green, kind: Lime, weight: 30 },
    { color: Green, kind: Lime, weight: 30 },
    { color: Yellow, kind: Lemon, weight: 30 },
    { color: Yellow, kind: Lemon, weight: 31 },
    { color: Yellow, kind: Lemon, weight: 32 }
];

let testHeavyApples = FruitBlender.onlyHeavyApples(Fruit.fruits) == [
    { color: Green, kind: Apple, weight: 30 },
    { color: Green, kind: Apple, weight: 32 },
];

let testHeavyRedApples = FruitBlender.onlyHeavyRedApples(Fruit.fruits) == [];
let testLightApples = FruitBlender.onlyLightApples(Fruit.fruits) == [
    { color: Green, kind: Apple, weight: 15 },
    { color: Red, kind: Apple, weight: 11 },
    { color: Red, kind: Apple, weight: 23 },
    { color: Green, kind: Apple, weight: 23 }
];

let testLightGreenApples = FruitBlender.onlyLightGreenApples(Fruit.fruits) == [
    { color: Green, kind: Apple, weight: 15 },
    { color: Green, kind: Apple, weight: 23 }
];

Js.log("testRedApples " ++ boolToString(testRedApples));
Js.log("testGreenApples " ++ boolToString(testGreenApples));
Js.log("testOnlyApples " ++ boolToString(testOnlyApples));
Js.log("testOnlyLemons " ++ boolToString(testOnlyLemons));
Js.log("testOnlyHeavy " ++ boolToString(testHeavyFruits));
Js.log("testHeavyApples " ++ boolToString(testHeavyApples));
Js.log("testHeavyRedApples " ++ boolToString(testHeavyRedApples));
Js.log("testLightApples " ++ boolToString(testLightApples));
Js.log("testLightGreenApples " ++ boolToString(testLightGreenApples));


 /* 
testRedApples(onlyRedApples.length);
testGreenApples(onlyGreenApples.length);
testYellowLemons(onlyYellowLemons.length);
testOnlyApples(onlyApples.length);
testOnlyLemons(onlyLemons.length);
testHeavyFruits(onlyHeavyFruits.length);
testHeavyApples(onlyHeavyApples.length);
testHeavyRedApples(onlyHeavyRedApples.length);
testLightApples(onlyLightApples.length);
testLightGreenApples(onlyLightGreenApples.length); 
*/