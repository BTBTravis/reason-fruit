type fruitPred = Fruit.fruit => bool;
let isOfKind = (k: Fruit.fruitKind) => (f: Fruit.fruit) => f.kind === k;
let isOfColor = (c: Fruit.fruitColor) => (f: Fruit.fruit) => f.color === c;

let heavy = 28;
type weight = 
    | Heavy
    | Light;

type isOfWeight = weight => Fruit.fruit => bool;
let isOfWeight: isOfWeight = w => f => {
    switch w {
        | Heavy => f.weight >= heavy
        | Light => f.weight < heavy
    };
};

type blender = fruitPred => list(Fruit.fruit) => list(Fruit.fruit); 
let blender = fn => fruits => List.filter(fn, fruits); 

let onlyRedApples = blender(isOfColor(Red));
let onlyGreenApples = blender((f: Fruit.fruit) => isOfColor(Green)(f) && isOfKind(Apple)(f));
let onlyYellowLemons = blender((f: Fruit.fruit) => isOfColor(Yellow)(f) && isOfKind(Lemon)(f));
let onlyApples = isOfKind(Apple) -> blender;
let onlyLemons = isOfKind(Lemon) -> blender;
let onlyHeavyFruits = isOfWeight(Heavy) -> blender;
let onlyHeavyApples = ((f: Fruit.fruit) => isOfWeight(Heavy)(f) && isOfKind(Apple)(f)) -> blender;
let onlyHeavyRedApples = ((f: Fruit.fruit) => isOfWeight(Heavy)(f) && isOfKind(Apple)(f) && isOfColor(Red)(f)) -> blender;
let onlyLightApples= ((f: Fruit.fruit) => isOfWeight(Light)(f) && isOfKind(Apple)(f)) -> blender;
let onlyLightGreenApples = ((f: Fruit.fruit) => isOfWeight(Light)(f) && isOfKind(Apple)(f) && isOfColor(Green)(f)) -> blender;
